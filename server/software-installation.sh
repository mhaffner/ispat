#!/bin/bash

# update system packages
yum update

# install some necessary utilities
yum install yum-utils device-mapper-persistent-data lvm2 git

# add official docker community edition repo
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# install docker community edition
yum install docker-ce

# start docker as a service
systemctl start docker

# start docker on boot
systemctl enable docker

# clone git repository
git clone git@gitlab.com:mhaffner/ispat.git ispat-code/
