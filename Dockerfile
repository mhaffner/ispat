FROM rocker/shiny

MAINTAINER Matthew Haffner <haffnerm@uwec.edu>

# install required packages then delete the tempoary files associated
RUN Rscript -e "install.packages(c('shiny','shinythemes','spatstat'), repos='https://cran.rstudio.com/')" \
&& rm -rf /tmp/downloaded_packages/ /tmp/*.rds

# this assumes you are running docker from the ispat/app/ directory
COPY ./app /srv/shiny-server/ispat/
